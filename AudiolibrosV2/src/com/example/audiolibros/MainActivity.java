package com.example.audiolibros;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;

import com.examle.audiolibros.fragments.DetalleFragment;
import com.examle.audiolibros.fragments.SelectorFragment;
import com.examle.audiolibros.fragments.SelectorFragment.OnGridViewListener;

public class MainActivity extends FragmentActivity  implements OnGridViewListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		/**Si existe el fragment container en pantalla peque�a, y si esta siendo creada por 
		 * primera vez.
		 * En tablets se cargan 2 fragments, en mobile 1*/
		if (findViewById(R.id.fragment_container) != null && savedInstanceState == null) {
			/** 
			 * creamos la primera instancia del fragment a a�adir*/
			SelectorFragment primerFragment = new SelectorFragment();
			getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, primerFragment).commit();
		}
		
		SelectorAdapter.inicializarVector();
		/*
		GridView gridview = (GridView) findViewById(R.id.gridview);
		gridview.setAdapter(new SelectorAdapter(this));
		gridview.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> parent, View v, int position, long id){
				Toast.makeText(MainActivity.this, "seleccionado el elemento: " + position, Toast.LENGTH_SHORT).show();
			}
		});*/
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	/**
	 * Se llama a este metodo cuando se pulse en un libro.
	 * para abrir el detalle, comprobamos si hay 1 o 2 fragments.
	 * Si es solo uno, sustituimos el actual por el nuevo fragment*/
	@Override
	public void onItemSelected(int position) {
		// TODO Auto-generated method stub
		
		DetalleFragment detalleFragment = (DetalleFragment) getSupportFragmentManager().findFragmentById(R.id.detalle_fragment);
		
		if (detalleFragment != null) {
			
			detalleFragment.updateBookView(position);
		}else{
			DetalleFragment nuevoFragment = new DetalleFragment();
			Bundle args = new Bundle();
			args.putInt(DetalleFragment.ARG_POSITION, position);
			nuevoFragment.setArguments(args);
			
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			
			transaction.replace(R.id.fragment_container,nuevoFragment);
			
			/**
			 * guardamos vista en pila, si el usuario pulsa atras, que no se detenga la aplicacion**/
			transaction.addToBackStack(null);
			transaction.commit();
			
		}
		
	}

}
