package com.example.audiolibros;

import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SelectorAdapter extends BaseAdapter {

	LayoutInflater layoutInflater;
	public static Vector<BookInfo> bookVector;
	
	public SelectorAdapter(Activity a){
			layoutInflater = (LayoutInflater) a.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			inicializarVector();
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return bookVector.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		/**Cuando se llame a getView, la primera vista sera nula, y habra que crear la vista desde cero
		 *  para los siguientes se pasa la vista ya creada, solo habra que modificarla y devolverla
		 **/
		ImageView imageView;
		TextView audiolibroTextView;
		BookInfo bookInfo = bookVector.elementAt(position);
		View view = convertView;
		
		if (convertView == null) {
			view = layoutInflater.inflate(R.layout.elemento_selector, null);
		}
		
		audiolibroTextView = (TextView) view.findViewById(R.id.titulo);
		imageView = (ImageView) view.findViewById(R.id.imageView1);
		imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		audiolibroTextView.setText(bookInfo.name);
		
		return view;
	}
	
	public static void inicializarVector(){
		
		bookVector = new Vector<BookInfo>();
		
		bookVector.add(
				new BookInfo("Kappa", "Akutagawa",R.drawable.kappa,
						"http://www.leemp3.com/leemp3/1/kappa_akutagawa.mp3"));
		
		bookVector.add(
				new BookInfo("Kappa", "Akutagawa",R.drawable.kappa,
						"http://www.leemp3.com/leemp3/1/kappa_akutagawa.mp3"));
		
		bookVector.add(
				new BookInfo("Kappa", "Akutagawa",R.drawable.kappa,
						"http://www.leemp3.com/leemp3/1/kappa_akutagawa.mp3"));
		
		bookVector.add(
				new BookInfo("Kappa", "Akutagawa",R.drawable.kappa,
						"http://www.leemp3.com/leemp3/1/kappa_akutagawa.mp3"));
		
		bookVector.add(
				new BookInfo("Kappa", "Akutagawa",R.drawable.kappa,
						"http://www.leemp3.com/leemp3/1/kappa_akutagawa.mp3"));
		
		bookVector.add(
				new BookInfo("Kappa", "Akutagawa",R.drawable.kappa,
						"http://www.leemp3.com/leemp3/1/kappa_akutagawa.mp3"));
	}

	

}
