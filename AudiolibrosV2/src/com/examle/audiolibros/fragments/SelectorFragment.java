package com.examle.audiolibros.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.GridView;
import android.widget.Toast;

import com.example.audiolibros.R;
import com.example.audiolibros.SelectorAdapter;

public class SelectorFragment extends Fragment {

	Activity actividad;
	GridView gridview;
	SelectorAdapter adaptador;
	OnGridViewListener mCallback;
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		actividad=activity;
		
		try {
			mCallback = (OnGridViewListener) activity;
		} catch (ClassCastException  e) {
			// TODO: handle exception
			throw new ClassCastException(activity.toString());
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		
		View inflatedView = inflater.inflate(R.layout.fragment_selector, container, false);
		
		gridview = (GridView) inflatedView.findViewById(R.id.gridview);
		adaptador= new SelectorAdapter(actividad);
		gridview.setAdapter(adaptador);
		gridview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, int position, long id){
				mCallback.onItemSelected(position);
				Toast.makeText(actividad, "seleccionado el elemento: "+position, Toast.LENGTH_SHORT).show();
			}
		});
		
		gridview.setOnItemLongClickListener(new OnItemLongClickListener() {
			public boolean onItemLongClick(AdapterView<?> parent, View v, final int position , long id){
				AlertDialog.Builder builder = new AlertDialog.Builder(actividad);
				CharSequence[] items = {"Compartir", "Borrar", "Insertar"};
				
				builder.setItems(items, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						switch(which){
						case 0:
							Toast.makeText(actividad, "Caso 0" , Toast.LENGTH_LONG).show();
							break;
						case 1:
							SelectorAdapter.bookVector.remove(position);
							adaptador.notifyDataSetChanged();
							break;
							
						case 2:
							SelectorAdapter.bookVector.add(SelectorAdapter.bookVector.elementAt(0));
							adaptador.notifyDataSetChanged();
						}
					}
				});
				builder.create().show();
				return true;
			}
		});
		
		
		
		return inflatedView;
	}
	
	/**
	 * Pasamos las pulsaciones asociadas al GridView a la actividad asociada, implementando la interfaz 
	 * OnGridViewLister en la Actidad que gestiona el fragment
	 * */
	public interface OnGridViewListener{
		public void onItemSelected(int position);
	}
}
