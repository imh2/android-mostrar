package com.examle.audiolibros.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.audiolibros.BookInfo;
import com.example.audiolibros.R;
import com.example.audiolibros.SelectorAdapter;

public class DetalleFragment extends Fragment {
	
	public static String ARG_POSITION= "position";
	Activity actividad;
	
	public void onAttach(Activity activity){
		super.onAttach(activity);
		actividad = activity;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		View inflatedView = inflater.inflate(R.layout.fragment_detalle, container, false);
		
		Bundle args = getArguments();
		
		if (args != null) {
			
			int position = args.getInt(ARG_POSITION);
			setUpBookInfo(position, inflatedView);
		}else {
			setUpBookInfo(0, inflatedView);
		}
		
		return inflatedView;
		
	};
	
	public void setUpBookInfo(int position, View view){
		BookInfo bookInfo = SelectorAdapter.bookVector.elementAt(position);
		TextView textView = (TextView) view.findViewById(R.id.textView1);
		TextView audiolibroTextView = (TextView) view.findViewById(R.id.textView2);
		ImageView imageView = (ImageView) view.findViewById(R.id.imageView1);
		
		imageView.setImageResource(bookInfo.resourceImage);
		textView.setText(bookInfo.autor);
		audiolibroTextView.setText(bookInfo.name);
		
		/*
		try {
			mediaPlayer.stop();
			mediaPlayer.release();
		} catch (Exception e) {
			// TODO: handle exception
		}*/
	};
	
	public void updateBookView(int position){
		setUpBookInfo(position, getView());
	}
	

}
