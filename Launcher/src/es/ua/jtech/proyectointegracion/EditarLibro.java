package es.ua.jtech.proyectointegracion;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;


public class EditarLibro extends Activity {

	EditText titulo,autor,paginas,isbn;
	int position;
	
	private Intent dataIntent;
	private Bundle myBundle;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_editar_libro);
		
		dataIntent = getIntent();
        myBundle = dataIntent.getExtras();
        
		position = myBundle.getInt("position");
		
		 
		if (position != -1)
		{
			
        titulo = (EditText)findViewById(R.id.titulo);
        autor = (EditText)findViewById(R.id.autor);
        isbn = (EditText)findViewById(R.id.isbn);
        paginas = (EditText)findViewById(R.id.numeroPaginas);
           
    	titulo.setText(myBundle.getString("titulo"));
		autor.setText(myBundle.getString("autor"));
		isbn.setText(myBundle.getString("isbn"));
		paginas.setText(Integer.toString(myBundle.getInt("paginas")));
		 
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_editar_libro, menu);
		return true;
	}
	
	 public boolean onOptionsItemSelected(MenuItem item) {
	    	
	    	titulo = (EditText)findViewById(R.id.titulo);
            autor = (EditText)findViewById(R.id.autor);
            isbn = (EditText)findViewById(R.id.isbn);
            paginas = (EditText)findViewById(R.id.numeroPaginas);
            
           switch (item.getItemId()) {
	      
	        case R.id.opcionguardar:
	        	
	        	if (position!=-1)
	        	{
		           
	        		dataIntent = new Intent();
        			
	        		dataIntent.putExtra("autor", autor.getText().toString());
	        		dataIntent.putExtra("titulo", titulo.getText().toString());
	        		dataIntent.putExtra("isbn", isbn.getText().toString());
	        		dataIntent.putExtra("paginas", Integer.parseInt( paginas.getText().toString()));
	       		  
	        		dataIntent.putExtra("position", position);
	        		
	       		  	setResult(RESULT_OK, dataIntent);
	       		  	
	        		super.finish();
		        	
	        	}else{
	        		
	        		dataIntent = new Intent();
        			
	        		dataIntent.putExtra("autor", autor.getText().toString());
	        		dataIntent.putExtra("titulo", titulo.getText().toString());
	        		dataIntent.putExtra("isbn", isbn.getText().toString());
	        		dataIntent.putExtra("paginas", Integer.parseInt( paginas.getText().toString()));
	       		  
	       		  	setResult(RESULT_OK, dataIntent);
	       		  	
	        		super.finish();
	        	}
	        	
	        	break;
	        
	        case R.id.opcioncancelar:
	        	super.onBackPressed();
	        	
	        	break;
	        	
	        default:
	            return super.onOptionsItemSelected(item);
	        }
	        return true;
	 }
	

}
