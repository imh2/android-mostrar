package es.ua.jtech.proyectointegracion;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

public class AdaptadorLibros {
	private final static String BASE_DATOS = "bdlibros.db";
	private final static String TABLA = "libros";
	private final static String[] COLUMNAS = {"_id", "titulo", "autor", "isbn", "paginas"};
	private final static int VERSION = 1;
	
	private  static final  String CREAR_BASE_DATOS = "create table " +
			TABLA + " (" + COLUMNAS[0] +
	        " integer primary key autoincrement, " +
	        COLUMNAS[1] + " text not null, " +
	        COLUMNAS[2] + " text not null, " +
	        COLUMNAS[3] + " text not null, " +
	        COLUMNAS[4] + " integer not null);";

	private static final String INSERT = "insert into " + TABLA + "(" + COLUMNAS[1] + ","
		    + COLUMNAS[2] + "," + COLUMNAS[3] + "," + COLUMNAS[4] + ") values (?,?,?,?)";
	
	
	private Context context;
	private SQLiteDatabase db;
	private SQLiteStatement insertStatement;
	

	public AdaptadorLibros(Context context) {
	      this.context = context;
	      LibrosHelper openHelper = new LibrosHelper(this.context);
	    
	      try {
	    	    db = openHelper.getWritableDatabase();
	    	} catch (SQLiteException ex) {
	    	    db = openHelper.getReadableDatabase();
	    	}
	      
	      this.insertStatement = this.db.compileStatement(INSERT);
	   }
	
	public int deleteAll() {
		  return db.delete(TABLA, null, null);
	   }
	

	public Cursor query() {
			return db.query(TABLA, COLUMNAS, null, null, null, null, null);
		}
	
	public Cursor queryNombre (String strTitulo){
		
		String selection = "titulo like ?";
		String sortOrder = "titulo DESC";
		String[] selectionArgs = new String[]{ "%" + strTitulo + "%"};
		
		return db.query(TABLA, COLUMNAS, selection, selectionArgs, null, null, sortOrder);
	}
	
	
	public Cursor queryIsbn (String strISBN){
		
		String selection = "isbn like ?";
		String sortOrder = "isbn DESC";
		String[] selectionArgs = new String[]{ "%" + strISBN + "%"};
		
		return db.query(TABLA, COLUMNAS, selection, selectionArgs, null, null, sortOrder);
	}
	
	
	public long insertar(Libro libro) {
		
		  this.insertStatement.bindString(1, libro.getTitulo());
		  this.insertStatement.bindString(2, libro.getAutor());
		  this.insertStatement.bindString(3, libro.getIsbn());
		  this.insertStatement.bindDouble(4, libro.getNumPaginas());
		
		  return this.insertStatement.executeInsert();
	   }
	
	
	private static class LibrosHelper extends SQLiteOpenHelper {

		 LibrosHelper(Context context) {
	         super(context, BASE_DATOS, null, VERSION);
	      }

	      @Override
	      public void onCreate(SQLiteDatabase db) {
	    	  db.execSQL(CREAR_BASE_DATOS);
	      }

	      @Override
	      public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	         db.execSQL("DROP TABLE IF EXISTS " + TABLA);
	         onCreate(db);
	      }
	   }

	
	public boolean actualizar(long fila, Libro libro) {
	   
	    ContentValues valoresActualizar = new ContentValues();
	          
	    valoresActualizar.put(COLUMNAS[1], libro.getTitulo());
	    valoresActualizar.put(COLUMNAS[2], libro.getAutor());
	    valoresActualizar.put(COLUMNAS[3], libro.getIsbn());
	    valoresActualizar.put(COLUMNAS[4], libro.getNumPaginas());
	          
	    String where = COLUMNAS[0] + "=" + fila;
	          
	    db.update(TABLA, valoresActualizar, where, null);
	          
	    return true;
	}
	
	public int delete(long fila) {
	    String where = COLUMNAS[0] + "=" + fila;
	    return db.delete(TABLA, where, null);
	}
	
}
