package es.ua.jtech.proyectointegracion;

import java.util.ArrayList;

import android.content.Context;
import android.widget.Toast;

public class SingletonLibros {

    static private SingletonLibros singleton = new SingletonLibros();
    static private ArrayList<Libro> libros;
    
    private SingletonLibros() { 
    	libros = new ArrayList<Libro>();
    	libros.add(new Libro("Autor1", "12345", 123, "Titulo1"));
    	libros.add(new Libro("Autor2", "12346", 123, "Titulo2"));
    	
    }

    static public SingletonLibros getSingleton() {
        return singleton;
    }

    public ArrayList<Libro> getLibros(Context context) {
    	Toast.makeText(context,"Accediendo a los servicios remotos", Toast.LENGTH_LONG).show();
    	Listado.inicializado = true;
        return libros;
    }
    
    public ArrayList<Libro> borrarLibro(int pos) {
    	libros.remove(pos);
        return libros;
    }
    
    public ArrayList<Libro> modificarLibro(int pos, String editAutor,String editTitulo, String editIsbn, int editNumPaginas) {
    	
    	Libro editLibro = new Libro(editAutor,editIsbn,editNumPaginas,editTitulo);
    
    	libros.set(pos, editLibro);
    	
        return libros;
    }

  public ArrayList<Libro> addLibro(Libro nuevoLibro) {
    	
    	libros.add(nuevoLibro);
    	
        return libros;
    }


}
	
