package es.ua.jtech.proyectointegracion;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;


import android.os.AsyncTask;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.ListActivity;
import android.app.SearchManager;

public class Listado extends ListActivity  {
	
	AdaptadorLibros adaptadorLibros;
	
	public static boolean inicializado = false;
	
	private final static int altaLibro  = 1;
	
	private String titulo,autor,isbn;
	private int paginas;
	
	LibroAdapter la;
	Cursor cursor;

	private Intent dataIntent;
	private Bundle myBundle;
	
	public static String BUSCAR_TITULO;
	boolean buscarTitulo = false;
	
	private class LibroAdapter extends CursorAdapter {
		 
	    private Cursor items;
	    Map<String, CargarImagenTask> imagenesCargando ;
	    
	    public LibroAdapter(Context context, Cursor items) {
	        super(context, items);
	        
	        this.items = items;
	        imagenesCargando = new HashMap<String, Listado.CargarImagenTask>();
	    }
	 
	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        View v = convertView;
	        
	        if (v == null) {
	            LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            v = vi.inflate(R.layout.librolistado, null);
	            
	        }
	        
	        this.items.moveToPosition(position);       
	       
	        if (this.items != null) {
	        	
	            TextView tt = (TextView) v.findViewById(R.id.titulo);
	            TextView bt = (TextView) v.findViewById(R.id.autor);
	            ImageView portada = (ImageView) v.findViewById(R.id.icono);
	            
	           
	            
	            if (tt != null) {
	                tt.setText(items.getString(1));                          
	            }
	            if(bt != null){
	                bt.setText(items.getString(2));
	            }
	            if (portada != null) {
	            
	            	String isbn = (items.getString(3));
	   
	            	if (imagenesCargando.get(isbn)==null)
	            	{
	            		
	            		cargarImagen(isbn);
	            	}else
	            		if (imagenesCargando.get(isbn).getPortada()!=null)
	            		portada.setImageBitmap(imagenesCargando.get(isbn).getPortada());
	            		else
	            			portada.setImageResource(R.drawable.icon);
	            }
	            	
	         
	        }
	        return v;
	    }
	    
	    private void cargarImagen(String isbn) {
	        // Solo carga la imagen si no esta siendo cargada ya
	        if(imagenesCargando.get(isbn)==null) {
	          CargarImagenTask task = new CargarImagenTask();
	          imagenesCargando.put(isbn, task);
	          task.execute(isbn);
	        }
	      }

		@Override
		public void bindView(View arg0, Context arg1, Cursor arg2) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public View newView(Context arg0, Cursor arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			return null;
		}
	   
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		 requestWindowFeature(Window.FEATURE_PROGRESS);
		
		 this.getListView().setEmptyView(this.findViewById(android.R.id.empty));
		 
		//setContentView(R.layout.librolistado);
		 System.out.println("Oncreate del listado");
		
		adaptadorLibros = new AdaptadorLibros(this);
		System.out.println("Pista 1");
		/*
		if (!inicializado){
			
			adaptadorLibros.deleteAll();
			
			final ArrayList libros = AdaptadorREST.getLibrosREST().getLibros(this);
		
			
			for (int i=0; i<libros.size();i++){
				
				Libro miLibro = (Libro) libros.get(i);
				Log.e(" miLibro.getAutor()**", miLibro.getAutor());
				adaptadorLibros.insert(miLibro.getTitulo(), miLibro.getAutor(), miLibro.getIsbn(), miLibro.getNumPaginas());
			}
		}
	*/
		//adaptadorLibros.deleteAll();
		
		System.out.println("Pista 2");
		Intent intent = getIntent();

		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
		    String query = intent.getStringExtra(SearchManager.QUERY);
		 
		    Bundle appData = getIntent().getBundleExtra(SearchManager.APP_DATA);
		    boolean buscarTitulo = true;
		    if (appData != null) {
		        buscarTitulo = appData.getBoolean(Listado.BUSCAR_TITULO);
		    }
		    // Crear el cursor a partir de la query oportuna
		    if (buscarTitulo)
		   
		    cursor = adaptadorLibros.queryNombre(query);
		    
		    else
		    	
		    cursor = adaptadorLibros.queryIsbn(query);
		    	
		} else {
		    // Crear el cursor a partir de la lista completa de libros
			System.out.println("Pista 3");
			cursor=adaptadorLibros.query();
			System.out.println("Pista 4");
		}
			
		System.out.println("antes de la primera consulta al cursor");
		if (cursor.getCount() <= 0) {
		    // Lanza la tarea del servicio web
			System.out.println("antes de llamar al los servicios externos");
			inicializado = true;
			new CargarLibrosTask().execute();

		}else	
		if (cursor.moveToFirst()) {
			System.out.println("Ya se ha accesiso al servicio, la bd tiene contenido");
			do {
				Log.i("Tabla Items", cursor.getString(1)  + " " + cursor.getString(2)  + " "  + cursor.getString(3)   + " " + cursor.getString(4));
			} while (cursor.moveToNext());
		}
		
		la = new LibroAdapter(this,cursor);
		
		setListAdapter(la);
		
		System.out.println("Antes de llamar a la vista");
		ListView list = getListView();
		
		list.setOnItemClickListener(new OnItemClickListener() {
		    public void onItemClick(AdapterView<?> parent, View view,
		    int position, long id) {
	
		    	dataIntent = new Intent(Listado.this,VisualizarLibro.class);
		    	 
		    	myBundle = new Bundle();
		    	
		    	cursor.moveToPosition(position);
		    					
		    	myBundle.putInt("position", position);
		    	
		    	myBundle.putInt("id", cursor.getInt(0));
		    	myBundle.putString("titulo", cursor.getString(1));
		    	myBundle.putString("autor", cursor.getString(2));
		    	myBundle.putString("isbn", cursor.getString(3));
		    	myBundle.putInt("paginas", cursor.getInt(4));
		    
		    	dataIntent.putExtras(myBundle);
				
	        	startActivity(dataIntent);
		    	
		    }
		});
		
		
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menulibrolistado, menu);
		return true;
	}

	
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
    	
        switch (item.getItemId()) {
        case R.id.opcionaltalibro:

        	dataIntent = new Intent(this, EditarLibro.class);
	    	 
        	myBundle = new Bundle();
			
        	myBundle.putInt("position", -1);
	    
	    	dataIntent.putExtras(myBundle);
		
			startActivityForResult(dataIntent, altaLibro);
        	
        	break;
        case R.id.opcionbuscarnombre:
        	buscarTitulo = true;
        	onSearchRequested();
        	break;
        case R.id.opcionbuscarisbn:
        	buscarTitulo = false;
        	onSearchRequested();
        	break;
        default:
            return super.onOptionsItemSelected(item);
        }
        return true;
    }
    
    @Override
    public boolean onSearchRequested() {
        Bundle appData = new Bundle();
        appData.putBoolean(Listado.BUSCAR_TITULO, buscarTitulo);
        startSearch(null, false, appData, false);
        return true;
    }
    
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
        
        if(resultCode==RESULT_OK){
        	
        	switch (requestCode) {
        	
			case altaLibro:
				
				myBundle = data.getExtras();
			   
			    titulo= myBundle.getString("titulo");
			    autor = myBundle.getString("autor");
			    isbn = myBundle.getString("isbn");
			    paginas = myBundle.getInt("paginas");
			    	
			    JSONObject libroJson = new JSONObject();
				try {
					libroJson.put("@titulo", titulo);
					libroJson.put("@autor", autor);
					libroJson.put("@isbn", isbn);
					libroJson.put("@numPaginas", paginas);
					
					new TareaEnviarDatosLibro().execute(libroJson.toString());
				} catch (JSONException e) {
					e.printStackTrace();
				}
			    
			    	
				break;
	
			default:
				break;
			}  
        }
    }
    
    @Override
 		public void onResume(){
 			super.onResume();
 			
 			cursor.requery();
	    	la.notifyDataSetChanged();
 			
 		}
    
    
    
	private List<Libro> parseLibros(Reader reader) {

		List<Libro> listaLibros = new ArrayList<Libro>();
		
		try {
			System.out.println("En el parser");
  		  
   		 XmlPullParserFactory parserCreator = XmlPullParserFactory.newInstance();
   		 
   		parserCreator.setNamespaceAware(true);
   		  
   		 XmlPullParser parser = parserCreator.newPullParser();
   		 
   		 parser.setInput(reader);
   		  
   		 int parserEvent = parser.getEventType();
   		 
   		 while (parserEvent != XmlPullParser.END_DOCUMENT) {
   		 
   		    switch (parserEvent) {
   		    case XmlPullParser.START_DOCUMENT:
   		      break;
   		    case XmlPullParser.END_DOCUMENT:
   		      break;
   		    case XmlPullParser.START_TAG:
   		    
   		    	String tag = parser.getName();
   		    	
   		       if(tag.equalsIgnoreCase("libro")) {
	    		      
   		    	   String autor = parser.getAttributeValue(null, "autor");
	    		        String isbn = parser.getAttributeValue(null, "isbn");
	    		        String numPaginas = parser.getAttributeValue(null, "numPaginas");
	    		        String titulo = parser.getAttributeValue(null, "titulo");
	    		        Libro lib=new Libro(autor, isbn, Integer.parseInt(numPaginas),titulo);
	    		        adaptadorLibros.insertar(lib);

   		       	}
   		      break;
   		    case XmlPullParser.END_TAG:
   		      break;
   		    }
   		 
   		    parserEvent = parser.next();
   		  }
   		 
   		} catch (Exception e) {
   		  Log.e("Net", "Error en la conexion de red", e);
   		}
		return listaLibros;
	}

	class CargarLibrosTask extends AsyncTask<String, Integer, List<Libro>> {

		@Override
		protected List<Libro> doInBackground(String... params) {

			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet(
					"http://server.jtech.ua.es/jbib-rest/resources/libros");
			
			request.setHeader("Accept", "application/xml");

			try {
				ResponseHandler<String> handler = new BasicResponseHandler();
				String contenido = client.execute(request, handler);
				
				return parseLibros(new StringReader(contenido));
				
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				client.getConnectionManager().shutdown();
			}

			return null;
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			setProgressBarIndeterminateVisibility(false);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			setProgressBarIndeterminateVisibility(true);
		}

		@Override
		protected void onPostExecute(List<Libro> result) {
			setProgressBarIndeterminateVisibility(false);
			System.out.println("onPostExecute");
			if (result != null) {
				cursor.requery();
				la.notifyDataSetChanged();
			} else {
				Toast.makeText(Listado.this,
						"Error al cargar libros", Toast.LENGTH_LONG).show();
			}
		}
	}
    
	public Bitmap getPortadaLibro(String isbn) {
	    Bitmap portada = null;
	 
	    String urlToSendRequest =
	            "http://server.jtech.ua.es/jbib-rest/resources/libros/"
	            + isbn.trim() + "/imagen";
	 
	    DefaultHttpClient httpClient = new DefaultHttpClient();
	    HttpGet httpget = new HttpGet(urlToSendRequest);
	 
	    HttpResponse response;
	    try {
	        response = httpClient.execute(httpget);
	        portada = BitmapFactory.decodeStream(response.getEntity()
	                .getContent());
	    } catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	        httpClient.getConnectionManager().shutdown();
	    }
	 
	    return portada;
	}
	
	class CargarImagenTask extends AsyncTask<String, Integer, Bitmap> {
		 
	    Bitmap portada;
	 
	    @Override
	    protected Bitmap doInBackground(String... params) {
	        return getPortadaLibro(params[0]);
	    }
	 
	    @Override
	    protected void onPostExecute(Bitmap result) {
	        if (result != null) {
	            this.portada = result;
	            la.notifyDataSetChanged();
	        }
	    }
	 
	    public Bitmap getPortada() {
	        return portada;
	    }
	}
	
	private int enviarLibro(String json) throws ClientProtocolException, IOException {
		
		
		DefaultHttpClient client = new DefaultHttpClient();


		
		client.getCredentialsProvider().setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
			   new UsernamePasswordCredentials("bibliotecario", "bibliotecario"));
		
		HttpPost post = new HttpPost("http://server.jtech.ua.es/jbib-rest/resources/libros");
		StringEntity s = new StringEntity(json.toString());
		s.setContentType("application/json");
		post.setEntity(s);
		
		HttpResponse response = client.execute(post);
		
		return response.getStatusLine().getStatusCode();
	}

	class TareaEnviarDatosLibro extends AsyncTask<String, Integer, Integer> {

		@Override
		protected Integer doInBackground(String... params) {
			String json = params[0];
					
			try {
				return enviarLibro(json);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return -1;
		}

		@Override
		protected void onPostExecute(Integer result) {
			if(result==201) {
				Libro lib=new Libro(autor, isbn, paginas, titulo);
				adaptadorLibros.insertar(lib);
				finish();
			} else {
				Toast.makeText(Listado.this, "Error " + result.toString(), Toast.LENGTH_LONG).show();
			}
		}		
	}	
	
	
}



