package es.ua.jtech.proyectointegracion;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
/***
 * 
 * Si tengo que reiniciar el servidor
 * server: server.jtech.ua.es - root - login: el mismo que en proweb pero acabado en j
 * service tomcat stop
 * service tomcat start
 *
 */


public class Launcher extends Activity {
	
	private final static int altaLibro  = 1;
	private AdaptadorLibros adaptadorLibros;
	private String titulo,autor,isbn;
	private int paginas;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menulaunch,menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
    	Intent i;
    	
        switch (item.getItemId()) {
        
        case R.id.opcionlistado:
        	i = new Intent(this, Listado.class);
        	startActivity(i);
        	break;
        	
        case R.id.opcionalta:
        	
        	i = new Intent(this, EditarLibro.class);
	    	 
	    	Bundle myData = new Bundle();
			
	    	myData.putInt("position", -1);
	    
			i.putExtras(myData);
		
			startActivityForResult(i, altaLibro);
        	
        	break;	
        default:
            return super.onOptionsItemSelected(item);
        }
        return true;
    }
    
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
        if(resultCode==RESULT_OK){
        	
        	switch (requestCode) {
        	
			case altaLibro:
					Bundle myData = data.getExtras();
					
					adaptadorLibros = new AdaptadorLibros(this);
					
			    	titulo= myData.getString("titulo");
			    	autor = myData.getString("autor");
			    	isbn = myData.getString("isbn");
			    	paginas = myData.getInt("paginas");
			    	
			    	Libro lib=new Libro(autor, isbn, paginas, titulo);
			    	SingletonLibros.getSingleton().addLibro(lib);
			    	
			    	adaptadorLibros.insertar(lib);
			    	
				break;
	
			default:
				break;
			}  
        }
    }
}