package es.ua.jtech.proyectointegracion;

import android.graphics.Bitmap;

public class Libro {
	private String autor;
	private String isbn;
	private int numPaginas;
	private String titulo;
	private Bitmap portada;
	
	public Libro(String au, String is, int nu, String ti) {
		autor = au;
		isbn = is;
		numPaginas = nu;
		titulo = ti;
		
		portada =null;

	}
	
	public String getAutor() { return autor; }
	public String getIsbn() { return isbn; }
	public int getNumPaginas() { return numPaginas; }
	public String getTitulo() { return titulo; }
	public void setPortada(Bitmap por) { portada = por; }
	public Bitmap getPortada() { return portada; }
}
