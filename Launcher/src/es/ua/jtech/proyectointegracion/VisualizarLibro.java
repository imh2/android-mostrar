package es.ua.jtech.proyectointegracion;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class VisualizarLibro extends Activity {

	private TextView textViewTitulo,textViewAutor,textViewPaginas,textViewIsbn;
	private int position ;
	
	private String  titulo,autor,isbn;
	private int paginas,id;

	
	private Intent dataIntent;
	private Bundle myBundle;
	
	private final static int editLibro  = 2;
	
	private AdaptadorLibros adaptadorLibros;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_visualizar_libro);
		
		dataIntent = getIntent();
        myBundle = dataIntent.getExtras();
        
        textViewTitulo = (TextView)findViewById(R.id.titulo);
        textViewAutor = (TextView)findViewById(R.id.autor);
        textViewIsbn = (TextView)findViewById(R.id.isbn);
        textViewPaginas = (TextView)findViewById(R.id.numeropaginas);
        
       
      
        id = myBundle.getInt("id");
        
        position = myBundle.getInt("position");
        
        adaptadorLibros = new AdaptadorLibros(this);
		
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_visualizar_libro, menu);
		return true;
	}
	
	 public boolean onOptionsItemSelected(MenuItem item) {
	        // Handle item selection
	    	
	        switch (item.getItemId()) {
	        case R.id.opcioneditar:
	        	
	        	dataIntent = new Intent(this, EditarLibro.class);
		    	 
	        	myBundle = new Bundle();
		    
	        	myBundle.putInt("position", position);
	        	
	        	myBundle.putString("titulo", titulo);
	        	myBundle.putString("autor", autor);
	        	myBundle.putString("isbn", isbn);
	        	myBundle.putInt("paginas", paginas);
		    	
		    	dataIntent.putExtras(myBundle);
		    	
		    	startActivityForResult(dataIntent,editLibro);

	        	break;
	        	
	        	
	        case R.id.opcionborrar:
	        
	        	SingletonLibros.getSingleton().borrarLibro(position);
	        	
	        	adaptadorLibros.delete(id);
	        	
	        	super.finish();

	        	break;
	        	
	       default:
	            return super.onOptionsItemSelected(item);
	       }
	        return true;
	 }

	 	
	   @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	        super.onActivityResult(requestCode, resultCode, data);
	        
	        
	        if(resultCode==RESULT_OK){
	        	
	        	switch (requestCode) {
	        	
				case editLibro:
					
						myBundle = data.getExtras();
											
				    	titulo = myBundle.getString("titulo");
				    	autor = myBundle.getString("autor");
				    	isbn = myBundle.getString("isbn");
				    	paginas = myBundle.getInt("paginas");
				    	
				    	SingletonLibros.getSingleton().modificarLibro(position,autor,titulo, isbn,paginas);
				    	
				    	ArrayList libros = SingletonLibros.getSingleton().getLibros(this);
				    	
				    	Libro miLibro = (Libro) libros.get(position);
				    	
				    	adaptadorLibros.actualizar(id, miLibro);
				    	
					break;
		
				default:
					break;
				}  
	        }
	    }
	    

	   @Override
		public void onResume(){
			super.onResume();
			
		      titulo = myBundle.getString("titulo");
		      autor = myBundle.getString("autor");
		      isbn = myBundle.getString("isbn");
		      paginas = myBundle.getInt("paginas");
		        
		      textViewTitulo.setText(titulo);
		      textViewAutor.setText(autor);
		      textViewIsbn.setText("ISBN : " + isbn);
		      textViewPaginas.setText("N�mero de p�ginas : " + Integer.toString(paginas));
			
		}
	 
}
