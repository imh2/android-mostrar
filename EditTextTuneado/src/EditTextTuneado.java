import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.EditText;


public class EditTextTuneado extends EditText {

	private Paint pincel;
	
	public EditTextTuneado(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		
		pincel= new Paint();
		pincel.setColor(Color.BLACK);
		pincel.setTextAlign(Paint.Align.RIGHT);
		float densidad = getResources().getDisplayMetrics().density;
		pincel.setTextSize(12*densidad); //12 dp, en funcion de la densidad.
	}
	
	protected void onDraw(Canvas canvas){
		Rect rect = new Rect();
		
		/**
		 * recorremos todas las lineas del textview
		 * Count-> cuantas lienas
		 * Bounds-> donde se dibuja la linea
		 * **/
		for (int i = 0; i < getLineCount(); i++) {
			int lineaBase = getLineBounds(i, rect);
			//subrayamos la linea
			canvas.drawLine(rect.left, lineaBase +2, rect.right , lineaBase+2, pincel);
			//pintamos el numero de linea a la derecha
			canvas.drawText("", (i+1), getWidth() - 2, pincel);
		}
		
		super.onDraw(canvas);
	}

}
